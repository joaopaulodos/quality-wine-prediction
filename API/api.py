from flask import Flask, request, jsonify, make_response
import joblib as jb
from flask_cors import CORS


regressor = jb.load("model/regressor_10072020.pkl.z")

app = Flask(__name__)

app.config["DEBUG"] = True




@app.route('/', methods=['OPTIONS','POST'])
def predict():

  if request.method == 'OPTIONS':
    return build_preflight_response()

  elif request.method == 'POST':
    features = []

    data = request.json
    data = data['features']
    print(data)
    features.append(float(data["fixed_acidity"]))
    features.append(float(data["volatile_acidity"]))
    features.append(float(data["citric_acid"]))
    features.append(float(data["residual_sugar"]))
    features.append(float(data["chlorides"]))
    features.append(float(data["free_sulfur_dioxide"]))
    features.append(float(data["total_sulfur_dioxide"]))
    features.append(float(data["density"]))
    features.append(float(data["pH"]))
    features.append(float(data["sulphates"]))
    features.append(float(data["alcohol"]))

    result = regressor.predict([features])

    return build_actual_response(jsonify({"RESULTADO":int(result)}))

def build_preflight_response():
    response = make_response()
    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add('Access-Control-Allow-Headers', "*")
    response.headers.add('Access-Control-Allow-Methods', "*")
    return response

def build_actual_response(response):
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response

app.run()






