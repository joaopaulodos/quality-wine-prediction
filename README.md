# Sistema de predição de qualidade de vinho

Sistema para predição da qualidade de um vinho a partir de suas caracteristicas. Para esse projeto foi utilizado o React para criação do frontend e integração com backend via api. Backend criado com Flask/Python.


Para criar as imagens execute o Dockerfile das pastas frontend e api.

Na api o comando é:

```
docker build -t wine_api .
```
No frontend o comando é:
```
docker build -t wine_frontend .
```


Para subir a imagem docker da API rode o seguinte comando:

```
docker run -d --network host  wine_api
```

Para subir a imagem docker do frontend rode o seguinte comando:

```
docker run -d -v ${PWD}:/app -v /app/node_modules -p 3001:3000 --rm wine_frontend
```

Caso positivo, o frontend deve apresentar o seguinte:

```
Compiled successfully!

You can now view hello-world in the browser.

  Local:            http://localhost:3000
  On Your Network:  http://172.17.0.2:3000

Note that the development build is not optimized.
To create a production build, use npm run build.

```

Note que o enereço de ip para acessar o frontend nesse caso será: http://172.17.0.2:3000
