import React from 'react';
import axios from 'axios';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import './fonts.css';

class App extends React.Component {

  state = {
    fixedAcidity: 0,
    volatileAcidity: 0,
    citricAcid: 0,
    residualSugar: 0,
    chlorides: 0,
    freeSulfurDioxide: 0,
    totalSulfurDioxide: 0,
    density: 0,
    pH: 0,
    sulphates: 0,
    alcohol: 0,
    quality: 0
  }

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value}, this.handleSubmit(event) );
  }

  handleSubmit = event => {
    event.preventDefault();

    const features = {
      "fixed_acidity":           this.state.fixedAcidity,
      "volatile_acidity":        this.state.volatileAcidity,
      "citric_acid":             this.state.citricAcid,
      "residual_sugar":          this.state.residualSugar,
      "chlorides":               this.state.chlorides,
      "free_sulfur_dioxide":     this.state.freeSulfurDioxide,
      "total_sulfur_dioxide":    this.state.totalSulfurDioxide,
      "density":                 this.state.density,
      "pH":                      this.state.pH,
      "sulphates":               this.state.sulphates,
      "alcohol":                 this.state.alcohol
    }

    const headers = {
      'Content-Type': 'application/json'
    }

    axios.post("http://localhost:5000/", {features},{ headers: headers })
      .then(res => {
        console.log(res);
        console.log(res.data);
        this.setState({"quality": res.data.RESULTADO})
      })
  }

  render() {
    console.log(this.state)
    return (
      <div className="App">

      <Container fluid="md">
        <Row style={{margin: "0px 10px 20px 30px"}}>

        </Row>
        <Row>
          <Col className="column-img">
            <div class="final-quality" style={{display:"flex", flexDirection: "column", }}>
              <p class="text-qualidade"> Descubra a  qualidade do seu vinho! </p>
            </div>
          </Col>

          <Col style={{height:"100vh",display:"flex", flexDirection: "column", justifyContent:"center", alignContent: "center", alignItems: "center"}}>



            <form onSubmit={this.handleSubmit}>
              <Row style={{display: "flex", flexDirection:"row", justifyContent: "space-between"}}>

                  <p>Acidez fixa:</p>
                  <input type="text" name="fixedAcidity" onChange={this.handleChange}  value={this.state.fixedAcidity} />

              </Row>

              <Row style={{display: "flex", flexDirection:"row", justifyContent: "space-between"}}>

                  <p>Acidez volátil:</p>
                  <input type="text" name="volatileAcidity" onChange={this.handleChange} value={this.state.volatileAcidity} />

              </Row>

              <Row style={{display: "flex", flexDirection:"row", justifyContent: "space-between"}}>

                  <p>Ácido Cítrico:</p>
                  <input type="text" name="citricAcid" onChange={this.handleChange} value={this.state.citricAcid}/>

              </Row>

              <Row style={{display: "flex", flexDirection:"row", justifyContent: "space-between"}}>

                  <p>Açúcar residual:</p>
                  <input type="text" name="residualSugar" onChange={this.handleChange} value={this.state.residualSugar}/>

              </Row>

              <Row style={{display: "flex", flexDirection:"row", justifyContent: "space-between"}}>

                  <p>Cloretos:</p>
                  <input type="text" name="chlorides" onChange={this.handleChange} value={this.state.chlorides}/>

              </Row>

              <Row style={{display: "flex", flexDirection:"row", justifyContent: "space-between"}}>

                  <p>Dióxido de enxofre livre:</p>
                  <input type="text" name="freeSulfurDioxide" onChange={this.handleChange} value={this.state.freeSulfurDioxide}/>

              </Row>

              <Row style={{display: "flex", flexDirection:"row", justifyContent: "space-between"}}>

                  <p>Dióxido de enxofre total:</p>
                  <input type="text" name="totalSulfurDioxide" onChange={this.handleChange} value={this.state.totalSulfurDioxide} />

              </Row>

              <Row style={{display: "flex", flexDirection:"row", justifyContent: "space-between"}}>

                  <p>Densidade:</p>
                  <input type="text" name="density" onChange={this.handleChange} value={this.state.density}/>

              </Row>

              <Row style={{display: "flex", flexDirection:"row", justifyContent: "space-between"}}>

                  <p>PH:</p>
                  <input type="text" name="pH" onChange={this.handleChange} value={this.state.pH}/>

              </Row>

              <Row style={{display: "flex", flexDirection:"row", justifyContent: "space-between"}}>

                  <p>Sulfatos:</p>
                  <input type="text" name="sulphates" onChange={this.handleChange} value={this.state.sulphates} />

              </Row>

              <Row style={{display: "flex", flexDirection:"row", justifyContent: "space-between"}}>

                  <p>Alcool:</p>
                  <input type="text" name="alcohol" onChange={this.handleChange} value={this.state.alcohol} />

              </Row>
              <Row style={{display:"flex", flexDirection:"column", alignContent: "center", justifyContent:"center", alignItems: "center", marginTop: "60px"}}>
                <p class="qualidade-result-label"> A qualidade do seu vinho é:</p>
                <p class="qualidade-result-value"> {this.state.quality} </p>
              </Row>




          </form>

          </Col>

        </Row>
      </Container>







      </div>
    );
  }
}

export default App;
